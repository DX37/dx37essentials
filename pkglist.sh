#!/bin/bash

if [ ! -x /usr/bin/git ]
then
    echo "Installing git..."
    apt-get update -qq
    apt-get install -y -qq git
    echo "git installed."
fi

REPO_COMMIT=https://gitlab.com/DX37/dx37essentials/commit
REPO_URL=https://dx37.gitlab.io/dx37essentials

X86_64_COMMIT_DESC=$(git log --pretty=format:"(%h) %s (%cD)" -n 1 -- public/x86_64)
X86_64_COMMIT_HASH=$(git log --pretty=format:"%H" -n 1 -- public/x86_64)
ARMV7_COMMIT_DESC=$(git log --pretty=format:"(%h) %s (%cD)" -n 1 -- public/armv7h)
ARMV7_COMMIT_HASH=$(git log --pretty=format:"%H" -n 1 -- public/armv7h)
AARCH64_COMMIT_DESC=$(git log --pretty=format:"(%h) %s (%cD)" -n 1 -- public/aarch64)
AARCH64_COMMIT_HASH=$(git log --pretty=format:"%H" -n 1 -- public/aarch64)

echo "<title>x86_64 pkglist</title>
<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">
<meta name=\"theme-color\" content=\"#1793d1\">
<a href=\"index.html\"><< Back</a><br>
<br>
<b>Latest change</b><br>
<a class=\"pkg\" target=\"_blank\" href=\"$REPO_COMMIT/$X86_64_COMMIT_HASH\">$X86_64_COMMIT_DESC</a><br>
<br>
<b>Current x86_64 packages list</b><br>" > public/pkglist-x86_64.html

cd public/x86_64
for pkg in `ls *.pkg.tar*`
do
    pkg_name=`echo $pkg | sed 's#.pkg.tar.zst##g' | sed 's#-x86_64##g' | sed 's#-any##g'`
    echo "<a class=\"pkg\" href=\"x86_64/$pkg\">$pkg_name</a><br>" >> ../pkglist-x86_64.html
done
cd ../..

echo "x86_64 packages list created."

echo "<title>armv7h pkglist</title>
<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">
<meta name=\"theme-color\" content=\"#1793d1\">
<a href=\"index.html\"><< Back</a><br>
<br>
<b>Latest change</b><br>
<a class=\"pkg\" target=\"_blank\" href=\"$REPO_COMMIT/$ARMV7_COMMIT_HASH\">$ARMV7_COMMIT_DESC</a><br>
<br>
<b>Current armv7h packages list</b><br>" > public/pkglist-armv7h.html

cd public/armv7h
for pkg in `ls *.pkg.tar*`
do
    pkg_name=`echo $pkg | sed 's#.pkg.tar.zst##g' | sed 's#-armv7h##g' | sed 's#-any##g'`
    echo "<a class=\"pkg\" href=\"armv7h/$pkg\">$pkg_name</a><br>" >> ../pkglist-armv7h.html
done
cd ../..

echo "armv7h packages list created."

echo "<title>aarch64 pkglist</title>
<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">
<meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">
<meta name=\"theme-color\" content=\"#1793d1\">
<a href=\"index.html\"><< Back</a><br>
<br>
<b>Latest change</b><br>
<a class=\"pkg\" target=\"_blank\" href=\"$REPO_COMMIT/$AARCH64_COMMIT_HASH\">$AARCH64_COMMIT_DESC</a><br>
<br>
<b>Current aarch64 packages list</b><br>" > public/pkglist-aarch64.html

cd public/aarch64
for pkg in `ls *.pkg.tar*`
do
    pkg_name=`echo $pkg | sed 's#.pkg.tar.zst##g' | sed 's#-aarch64##g' | sed 's#-any##g'`
    echo "<a class=\"pkg\" href=\"aarch64/$pkg\">$pkg_name</a><br>" >> ../pkglist-aarch64.html
done
cd ../..

echo "aarch64 packages list created."
