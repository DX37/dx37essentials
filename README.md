For use, add this to `/etc/pacman.conf`:

[dx37essentials]  
SigLevel = PackageOptional  
Server = https://dx37.gitlab.io/$repo/$arch
